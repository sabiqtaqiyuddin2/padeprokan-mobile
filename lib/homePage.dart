import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: Container(
        height: 2000,
        width: 2000,
        color: Color.fromARGB(255, 219, 223, 247),
        child: Container(
          height: 100,
          width: 100,
          color: Colors.white,
          margin: EdgeInsets.fromLTRB(0, 0, 0, 780),
        ),
      ),
    ));
  }
}
