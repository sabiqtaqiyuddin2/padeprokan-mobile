import 'package:flutter/material.dart';
import 'package:padepokan_mobile/homePage.dart';

void main() {
  runApp(const PadeprokanMobile());
}

class PadeprokanMobile extends StatelessWidget {
  const PadeprokanMobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}
